package live.limbs.cardsmobile.amqpback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AmqpbackApplication {

	public static void main(String[] args) {
		SpringApplication.run(AmqpbackApplication.class, args);
	}

}
