package live.limbs.cardsmobile.amqpback;

import live.limbs.cardsmobile.amqpback.data.AbstractBaseRequest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BaseRequestRepository extends JpaRepository<AbstractBaseRequest,String> {
}
