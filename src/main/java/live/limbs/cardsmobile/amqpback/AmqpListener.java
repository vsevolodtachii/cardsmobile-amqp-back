package live.limbs.cardsmobile.amqpback;

import com.fasterxml.jackson.databind.ObjectMapper;
import live.limbs.cardsmobile.amqpback.data.AbstractBaseRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;


@RabbitListener(queues = "main-queue")
public class AmqpListener {
    private Logger logger = LoggerFactory.getLogger(AmqpListener.class);

    @Autowired
    private ObjectMapper jacksonObjectMapper;

    @Autowired
    private BaseRequestRepository repository;

    private ThreadPoolExecutor executor =
            (ThreadPoolExecutor) Executors.newFixedThreadPool(4);


    private AbstractBaseRequest process(AbstractBaseRequest request) throws Exception {
        Thread.sleep(1000);
        return request;
    }

    @RabbitHandler
    public void receive(String in) {
        try {

            AbstractBaseRequest request = jacksonObjectMapper.readValue(in, AbstractBaseRequest.class);
            logger.info("New message received: " + request.toString());

            executor.submit(()->{
                try {
                    logger.info(repository.save(process(request)).getId() +" : is saved.");
                } catch (Exception e) {
                    logger.error(e.getMessage() ,e);
                }
            });
        } catch (IOException e) {
            logger.error(e.getMessage() ,e);
        }
    }
}
