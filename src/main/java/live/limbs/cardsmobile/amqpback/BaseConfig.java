package live.limbs.cardsmobile.amqpback;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class BaseConfig {
    @Bean
    public ObjectMapper getObjectMapper(){
        return new ObjectMapper();
    }

    @Bean
    public Queue hello() {
        return new Queue("main-queue");
    }

    @Bean
    public AmqpListener backProcessor(){
        return new AmqpListener();
    }
}
